//
//  LibraryView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 21.01.2023.
//

import SwiftUI
import JellyfinAPI


struct LibraryView: View {
    
    @EnvironmentObject var sessionData: SessionData
    
    @FetchRequest(sortDescriptors: [NSSortDescriptor(key: "year", ascending: false), NSSortDescriptor(key: "name", ascending: true)], predicate: nil) var albums: FetchedResults<DBAlbum>
    
    
    
    var body: some View {
        NavigationView{
            ScrollView{
                
                /*
                 if sessionData.cacheRefreshing {
                 Text("Caching")
                 } else {
                 */
                LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())]){
                    ForEach(albums, id: \.self){ album in
                        NavigationLink(destination:
                                        AlbumListView(album: album) ){
                            AlbumCardView(album: album, imageData: album.image?.data)
                                .padding(.all, 5.0)
                        }
                                        .buttonStyle(PlainButtonStyle())
                    }
                    
                }
                
                .padding(.bottom, 100)
                .padding(.horizontal, 12)
                //}
                
            }
            .task(){
                //if libraryItems == [] {await loadLibraryItems()}
            }
            .onReceive(sessionData.jellyfinClient.publisher) { _ in
                Task{
                    //if libraryItems == [] {await loadLibraryItems()}
                }
            }
            .navigationTitle("Library")
            
        }
            
    }
/*
    func loadLibraryItems() async {
         
        do{
            
            let response = try await sessionData.jellyfinClient?.send(Paths.getItems(parameters: Paths.GetItemsParameters(userID: sessionData.currentUserObject?.id, isRecursive: true, parentID: sessionData.currentLibrary, includeItemTypes: [BaseItemKind.musicAlbum] )))
            
            if response?.value.items == nil {
                print("Library request returned nil")
            }else {
                self.libraryItems = []
                for item in response!.value.items! {
                    // nested request to get images
                    var imageUrl = ""
                    if sessionData.jellyfinClient?.configuration.url.absoluteString != nil {
                        imageUrl = ((sessionData.jellyfinClient?.configuration.url.absoluteString)!) + Paths.getItemImage(itemID: item.id ?? "", imageType: "Primary").url
                    }
                    
                    self.libraryItems.append(LocalAlbum(imageURL: imageUrl, imageBlurHash: item.imageBlurHashes?.primary?.first?.value ?? "", name: item.name ?? "Unknown album", artist: item.artists?.first ?? "Unknown artist", id: item.id ?? "", genres: item.genres ?? []))
                }
            }
            
        }catch{
            print(error)
        }
    }
*/
    
}

struct LibraryView_Previews: PreviewProvider {
    static var previews: some View {
        LibraryView()
    }
}
