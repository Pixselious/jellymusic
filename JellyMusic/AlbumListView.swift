//
//  AlbumListView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 21.01.2023.
//

import SwiftUI
import JellyfinAPI
import Combine
import AVFoundation
import AudioToolbox

struct AlbumListView: View {
    
    @EnvironmentObject var sessionData: SessionData
    @State private var showingAlert = false
    @State private var alertError = "Default error"
    
    @State var album:DBAlbum
    @State var songs:Array<DBSong> = []
    
    var body: some View {
        ScrollView{
            
            //Album art
            if album.image?.data != nil {
                
                HStack{
                    Spacer()
                    ZStack {
                        //Shadow
                        VStack {
                            
                            Spacer()
                                .frame(height: 10)
                            
                            Image(uiImage: UIImage(data: album.image!.data!)!)
                                .resizable()
                                .scaledToFit()
                                .cornerRadius(7)
                                .frame(maxHeight: 210)
                                .blur(radius: 30, opaque: false)
                        }
                        
                        //Primary
                        Image(uiImage: UIImage(data: album.image!.data!)!)
                            .resizable()
                            .scaledToFit()
                            .cornerRadius(7)
                            .frame(maxHeight: 230)
                    }
                    .padding(.horizontal)
                    Spacer()
                }
            } else {
                ZStack {
                    
                    RoundedRectangle(cornerRadius: 7)
                        .foregroundColor(.gray)
                        .opacity(0.7)
                        .shadow(color: .gray, radius: 20, y:5)
                    
                    
                    Image(systemName: "record.circle.fill")
                        .resizable()
                        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                        .scaledToFit()
                        .foregroundColor(/*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/)
                }.frame(width: 230, height: 230)
            }
            // Album title
            Text(album.name!)
                .font(.title3)
                .fontWeight(.semibold)
                .foregroundColor(Color("AdaptiveBlack"))
                .multilineTextAlignment(.center)
                .padding(.top)
            //Album artist
            
            NavigationLink(destination: {
                ArtistView(artistId: album.artist?.id  ?? "Unknown artist", artistName: album.artist?.name ?? "Unknown artist")
            }){
                Text(album.artist?.name ?? "Unknown artist")
                    .font(.title3)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.center)
            }
            
            Spacer()
                .frame(height: 5)
            
            //Genres and year
            HStack(alignment: .center){
                if album.genres != nil  {
                    ForEach(album.genres!, id: \.self){item in
                        Text(item.uppercased())
                            .font(.subheadline)
                            .foregroundColor(Color.gray)
                        Text("⋅")
                            .font(.subheadline)
                            .foregroundColor(Color.gray)
                    }
                    
                }
                
                if album.year != -1{
                    Text(String(album.year))
                        .font(.caption)
                        .fontWeight(.semibold)
                        .foregroundColor(Color.gray)
                }
            }
            
            
            HStack(spacing: 20){
                
                Button(action: {
                    print("play")
                    Task{
                        await appendAndPlayFirst(shuffle: false)
                    }
                }){
                    ZStack{
                        Spacer()
                            .frame(height: 50)
                        
                        Text("\(Image(systemName: "play.fill")) Play")
                    }
                    .background(Color(.gray).opacity(0.2))
                    .cornerRadius(10)
                }
                
                Button(action: {
                    print("shuffle")
                    Task{
                        await appendAndPlayFirst(shuffle: true)
                    }
                }){
                    ZStack{
                        Spacer()
                            .frame(height: 50)
                        
                        Text("\(Image(systemName: "shuffle")) Shuffle")
                    }
                    .background(Color(.gray).opacity(0.2))
                    .cornerRadius(10)
                }
                
            }
            .padding(.horizontal, 20)
            .padding(.vertical)
            
            
            
            List(){
                
                ForEach(songs, id: \.id){ song in
                    Button(action: ({
                        Task{
                            await clearAndPlay(id: song.id!)
                        }
                    }), label: {Text(song.name!)})
                    .swipeActions(edge: .leading, allowsFullSwipe: true){
                        Button{
                            Task{await playNext(id: song.id!)}
                        } label: {
                            Label("Play next", systemImage: "square.3.stack.3d.top.filled")
                        }.tint(.purple)
                    }
                    .swipeActions(edge: .leading){
                        Button{
                            Task{await playLast(id: song.id!)}
                        } label: {
                            Label("Play last", systemImage: "square.3.stack.3d.bottom.filled")
                        }.tint(.orange)
                    }
                    
                }
                
                
            }
            .overlay(Divider(), alignment: .top)
            .listStyle(.inset)
            .frame(height: CGFloat(songs.count * 45))
            
            
            Rectangle()
                .frame(height: 100)
                .opacity(0)
            
            
        }
        .onAppear(){
            prepareSongs()
        }
    }
    
    
    func prepareSongs(){
        if !album.songs!.allObjects.isEmpty{
            self.songs = album.songs!.sortedArray(using: [NSSortDescriptor(key:"indexNumber", ascending:true)]) as! [DBSong]
        } else {
            print("No songs in album", album.songs!.allObjects)
        }
    }
    
    func playNext(id: String) async {
        
        do{
            var request = Paths.getUniversalAudioStream(itemID: id, parameters: Paths.GetUniversalAudioStreamParameters(
                container: ["aac", "m4a"],
                deviceID: sessionData.jellyfinClient?.configuration.deviceID,
                userID: sessionData.currentUserObject?.id,
                audioCodec: "aac",
                transcodingContainer: "ts",
                transcodingProtocol: "hls",
                enableRedirection: true
            ))
            
            request.query?.append(("api_key", sessionData.currentToken))
            
            let response = try await sessionData.jellyfinClient?.send(request)
            sessionData.firstInQ(url: response!.response.url!.absoluteString)
            
        }catch{
            print(error)
        }
    }
    
    func playLast(id: String) async {
        
        do{
            
            
            var request = Paths.getUniversalAudioStream(itemID: id, parameters: Paths.GetUniversalAudioStreamParameters(
                container: ["aac", "m4a"],
                deviceID: sessionData.jellyfinClient?.configuration.deviceID,
                userID: sessionData.currentUserObject?.id,
                audioCodec: "aac",
                transcodingContainer: "ts",
                transcodingProtocol: "hls",
                enableRedirection: true
            ))
            
            request.query?.append(("api_key", sessionData.currentToken))
            
            let response = try await sessionData.jellyfinClient?.send(request)
            
            
            //print( try await sessionData.jellyfinClient?.send(Paths.getVariantHlsAudioPlaylist(itemID: id, parameters: Paths.GetVariantHlsAudioPlaylistParameters(deviceProfileID: sessionData.jellyfinClient?.configuration.deviceID))).originalRequest?.allHTTPHeaderFields)
            //print( try await sessionData.jellyfinClient?.send(Paths.getVariantHlsAudioPlaylist(itemID: id, parameters: Paths.GetVariantHlsAudioPlaylistParameters(deviceProfileID: sessionData.jellyfinClient?.configuration.deviceID))).response.url?.absoluteString)
            //print(url)
            //print(sessionData.jellyfinClient!.configuration.url, Paths.getVariantHlsAudioPlaylist(itemID: id).url, sessionData.jellyfinClient!.accessToken)
            
            sessionData.appendToQ(url: response!.response.url!.absoluteString)
            //sessionData.audioPlayer.play()
            
        }catch{
            print(error)
        }
    }
    
    func clearAndPlay(id: String) async {
        
        do{
            
            
            var request = Paths.getUniversalAudioStream(itemID: id, parameters: Paths.GetUniversalAudioStreamParameters(
                container: ["aac", "m4a"],
                deviceID: sessionData.jellyfinClient?.configuration.deviceID,
                userID: sessionData.currentUserObject?.id,
                audioCodec: "aac",
                transcodingContainer: "ts",
                transcodingProtocol: "hls",
                enableRedirection: true
            ))
            
            request.query?.append(("api_key", sessionData.currentToken))
            
            let response = try await sessionData.jellyfinClient?.send(request)
            
            
            //print( try await sessionData.jellyfinClient?.send(Paths.getVariantHlsAudioPlaylist(itemID: id, parameters: Paths.GetVariantHlsAudioPlaylistParameters(deviceProfileID: sessionData.jellyfinClient?.configuration.deviceID))).originalRequest?.allHTTPHeaderFields)
            //print( try await sessionData.jellyfinClient?.send(Paths.getVariantHlsAudioPlaylist(itemID: id, parameters: Paths.GetVariantHlsAudioPlaylistParameters(deviceProfileID: sessionData.jellyfinClient?.configuration.deviceID))).response.url?.absoluteString)
            //print(url)
            //print(sessionData.jellyfinClient!.configuration.url, Paths.getVariantHlsAudioPlaylist(itemID: id).url, sessionData.jellyfinClient!.accessToken)
            
            sessionData.playNow(url: response!.response.url!.absoluteString)
            
            
        }catch{
            print(error)
        }
        
    }
    
    func appendAndPlayFirst(shuffle: Bool) async {
        
        var urls: [String] = []
        
        for song in songs {
            
            do{
                var request = Paths.getUniversalAudioStream(itemID: song.id ?? "no id", parameters: Paths.GetUniversalAudioStreamParameters(
                    container: ["aac", "m4a"],
                    deviceID: sessionData.jellyfinClient?.configuration.deviceID,
                    userID: sessionData.currentUserObject?.id,
                    audioCodec: "aac",
                    transcodingContainer: "ts",
                    transcodingProtocol: "hls",
                    enableRedirection: true
                ))
                
                request.query?.append(("api_key", sessionData.currentToken))
                
                let response = try await sessionData.jellyfinClient?.send(request)
                urls.append(response!.response.url!.absoluteString)
                
            }catch{
                print(error)
            }
        }
        
        sessionData.appendAndPlayFirst(urls: urls, shuffle: shuffle)
    }

}
/*
struct AlbumListView_Previews: PreviewProvider {
    static var previews: some View {
        AlbumListView(imageURL: "", imageBlurHash: "", title: "Title", artist: "Artist", id: "ID", genres: [])
    }
}
*/
