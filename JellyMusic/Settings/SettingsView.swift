//
//  SettingsView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 21.01.2023.
//

import SwiftUI
import Combine
import JellyfinAPI
import CoreData

struct SettingsView: View {
    
    @EnvironmentObject var sessionData: SessionData
    @Environment(\.managedObjectContext) var managedObjectContext
    
    @State private var showConnectSheet = false
    
    var body: some View {
        NavigationView{
            VStack{
                
                if sessionData.cacheRefreshing && sessionData.cacheTotalItems != 0 {
                    VStack {
                        Text("Caching")
                            .font(.title)
                        Text("\(Int(Float(sessionData.cacheCurrentItems) / (Float(sessionData.cacheTotalItems) / 100)))%")
                        Text("\(sessionData.cacheCurrentItems ) of \(sessionData.cacheTotalItems)")
                    }
                    
                        
                }
                
                Button{
                    if !sessionData.cacheRefreshing{
                        purgeAlbums()
                    } else {
                        print("not allowed to purge while refreshing")
                    }
                    
                } label: {
                    Text("Purge cache")
                        .foregroundColor(.red)
                }
                .navigationTitle("Settings")
                .toolbar(content: {
                    ToolbarItem(placement: .primaryAction, content: {
                        Button(action: {
                            showConnectSheet.toggle()
                        }, label: {Image(systemName: "server.rack")})
                    })})
            }
        }
        
        .sheet(isPresented: $showConnectSheet, content: {ConnectSheet()})
    }
    
    func purgeAlbums(){
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "DBAlbum")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try managedObjectContext.execute(deleteRequest)
            print("purged")
        } catch let error as NSError {
            print(error)
        }
    }
    
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
