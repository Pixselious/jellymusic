//
//  ContentView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 21.01.2023.
//

import SwiftUI
import Combine
import JellyfinAPI

struct Library: Hashable{
    var name : String
    var id : String
}

struct ConnectSheet: View {
    
    @EnvironmentObject var sessionData: SessionData
    
    @Environment(\.dismiss) var dismiss
    
    @State var server: String = ""
    @State var user: String = "test"
    @State var password: String = "test"
    
    @State var libraries: Array<Library> = []
    
    @State private var disposables = Set<AnyCancellable>()
    @State private var showingAlert = false
    @State private var alertError = "Default error"
    
    var body: some View {
        
        Form{
            
            if sessionData.isLoggedIn == false{
                
                Section("Server"){
                    TextField("Server URL", text: $server)
                        .disableAutocorrection(true)
                        .textContentType(.URL)
                }
                Section("User"){
                    TextField("Username", text: $user)
                        .disableAutocorrection(true)
                        .textContentType(.username)
                    TextField("Password", text: $password)
                        .disableAutocorrection(true)
                        .textContentType(.password)
                }
                Button(action: {Task{await connectToServer(uri: server, username: user, pw: password)}}, label: {Text("Connect")})
                
            } else {
                
                Section("Server"){
                    
                    Text(sessionData.currentServerURL ?? "Failed to load server name")
                    
                    Button(action: {
                        sessionData.isLoggedIn = false
                        sessionData.authResult = nil
                        self.libraries = []
                        /*
                                                     sessionData.publicServerInfo = nil
                                                     sessionData.userLibraries = []
                                                     sessionData.rawUserLibraries = nil*/
                        print("Disconnected")
                    }, label: {Text("Disconnect").foregroundColor(.red)})
                    
                }
                
                Section("Library"){
                    Picker("Displayed library", selection: $sessionData.currentLibrary) {
                        ForEach(self.libraries, id: \.self){ library in
                            Text(library.name).tag(library.id)
                        }
                    }.onAppear(){
                        Task{
                            await getLibraries()
                        }
                    }
                }
                
            }
        }
        
        .alert(alertError, isPresented: $showingAlert){
            Button("OK", role: .cancel) {}
        }
        
        
    }

    func connectToServer(uri: String, username: String, pw: String) async {
        
        let uriComponents = URLComponents(string: uri) ?? URLComponents()
        var uri = uriComponents.string ?? ""
        if uri.last == "/" {
            uri = String(uri.dropLast())
        }
        
        let jellyfinClient = JellyfinClient(configuration: JellyfinClient.Configuration(url: URL(string: uri)!, client: "JellyMusic", deviceName: "test", deviceID: "xcode-debug-persistent-id", version: "0.0.2"))
        
        //Pass this as a device id \(UIDevice.current.identifierForVendor!.uuidString)
        
        // Provided by JellyfinClient
        do{
            
            let response = try await jellyfinClient.signIn(username: username, password: pw)
            
            sessionData.jellyfinClient = jellyfinClient
            
            sessionData.isLoggedIn = true
            sessionData.currentUserName = response.user?.name
            sessionData.currentUserObject = response.user
            sessionData.currentToken = response.accessToken
            sessionData.currentServerURL = uri
            sessionData.currentServerID = response.serverID
            sessionData.currentSessionInfo = response.sessionInfo
            sessionData.authResult = response
            
        }catch{
            self.alertError = "\(error.localizedDescription)"
            self.showingAlert = true
            print(error)
        }
            
    }
    
    func getLibraries() async{
        
        
        do{
            
            let response = try await sessionData.jellyfinClient?.send(Paths.getUserViews(userID: sessionData.currentUserObject?.id ?? ""))
            
            if response != nil {
                
                for item in response!.value.items! {
                    self.libraries.append(Library(name: item.name ?? "No name", id: item.id ?? "No id"))
                }
                
                
            }else{
                self.alertError = "Request returned nil"
                self.showingAlert = true
                print("Request returned nil")
            }
            
        }catch{
            self.alertError = "\(error.localizedDescription)"
            self.showingAlert = true
            print(error)
        }
        
    }
    
}

struct ConnectSheet_Previews: PreviewProvider {
    static var previews: some View {
        ConnectSheet()
    }
}
