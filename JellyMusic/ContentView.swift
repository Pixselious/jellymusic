//
//  ContentView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 21.01.2023.
//

import SwiftUI
import JellyfinAPI
import AVFoundation
import MediaPlayer
import Combine

struct ContentView: View {
    
    @EnvironmentObject var sessionData: SessionData
    @Environment(\.managedObjectContext) var managedObjectContext
    @Environment(\.scenePhase) var scenePhase
    
    @State public var selectedTab = "Library"
    
    
    //Monitoring playback
    @State var timeObserverToken1: Any?
    @State var timeObserverToken2: Any?
    @State var timeObserverToken3: Any?
    
    @State var endNotification = NotificationCenter.default.publisher(for: .AVPlayerItemDidPlayToEndTime)
    
    @State var monitoredId: String?
    
    //Now Playing bar
    @State var isExpanded = false
    @Namespace var animation
    
    @State var nowPlayingItem:DBSong?
    @State var nowPlayingImage:Data?

    
    //Persistent data
    @AppStorage("is logged in") var persistentIsLoggedIn: Bool?
    @AppStorage("user") var persistentUser: String?
    @AppStorage("token") var persistentToken: String?
    @AppStorage("server") var persistentServer: String?
    @AppStorage("library") var persistentLibrary: String?
    
    
    //Caching
    @FetchRequest(sortDescriptors: [SortDescriptor(\.id)])
    var fetchedResults1: FetchedResults<DBAlbum>
    @FetchRequest(sortDescriptors: [SortDescriptor(\.id)])
    var fetchedResults2: FetchedResults<DBSong>
    @FetchRequest(sortDescriptors: [SortDescriptor(\.id)])
    var fetchedResults3: FetchedResults<DBSong>
    @FetchRequest(sortDescriptors: [SortDescriptor(\.id)])
    var fetchedResults4: FetchedResults<DBArtist>
    
    @State private var disposables = Set<AnyCancellable>()
    
    
    @State private var refreshID = UUID()
    private var didSave =  NotificationCenter.default.publisher(for:  .NSManagedObjectContextDidSave)
    
    @State private var refreshID2 = UUID()
    
    
    var body: some View {
        ZStack(alignment: .bottom){
            
            TabView(selection: $selectedTab) {
                LibraryView()
                    .tabItem {
                        Label("Library", systemImage: "square.stack.fill")
                    }
                    .tag("Library")
                    .id(refreshID)
                    .onReceive(self.didSave) { _ in   //the listener
                        //self.refreshID = UUID()
                        //print("generated a new UUID")
                    }
                
                SettingsView()
                    .tabItem {
                        Label("Settings", systemImage: "gearshape.fill")
                    }
                    .tag("Settings")
                    .task{
                        if !sessionData.cacheRefreshing{
                            sessionData.cacheRefreshing = true
                        }
                    }
            }
            
            MiniPlayerView()
                .onAppear(){
                    print("Appeared")
                }
            
            
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.didEnterBackgroundNotification), perform: { output in
            //Did enter background
            //Store new data
            persistentIsLoggedIn = sessionData.isLoggedIn
            persistentUser = sessionData.currentUserName
            persistentToken = sessionData.currentToken
            persistentServer = sessionData.currentServerURL
            persistentLibrary = sessionData.currentLibrary
        })
        .task {
            
            //Load persistent data
            
            sessionData.currentUserName = persistentUser
            sessionData.currentToken = persistentToken
            sessionData.currentServerURL = persistentServer
            sessionData.currentLibrary = persistentLibrary ?? ""
            
            //print(sessionData.isLoggedIn, sessionData.currentUser, sessionData.currentPassword, sessionData.currentServer, sessionData.currentLibrary)
            
            if sessionData.currentToken != nil && sessionData.currentServerURL != nil {
                Task{
                    await checkToken()
                }
                
            } else {
                tokenCheckFailed()
            }
            // TODO: if at lest one (except password) show onboarding card
            
            // Setup audio session
            do{
                try AVAudioSession.sharedInstance().setCategory(.playback)
            } catch {
                print("AVAudio setup: ", error)
            }
            
            
            sessionData.initEventListeners()
            
            
            setupRemoteTransportControls()
            addPeriodicTimeObserver()
            addPlaybackBegunBoundaryTimeObserver()
            
            setupNowPlaying()
        }
        

        .onChange(of: sessionData.cacheRefreshing){ state in
            if state {
                Task{
                    await refreshCache()
                }
            }
        }
        .onChange(of: sessionData.audioPlayer.currentItem?.getSourceUrl()){_ in
            print("CURRENT ITEM CHANGED")
            Task{
                //await getNowPlayingData()
            }
        }
        .onChange(of: sessionData.endPlaybackNotification){ _ in
            Task{
                await getNowPlayingData()
            }
            print("ended")
        }
        .onChange(of: scenePhase) { newPhase in
            if newPhase != .background {
                Task{
                    await getNowPlayingData()
                }
            }
        }
    }
    
    func checkToken() async {
        
        let jellyfinClient = JellyfinClient(configuration: JellyfinClient.Configuration(url: URL(string: sessionData.currentServerURL!)!, client: "JellyMusic", deviceName: "test", deviceID: "xcode-debug-persistent-id", version: "0.0.2"), accessToken: persistentToken!)
        
        //Pass this as a device id \(UIDevice.current.identifierForVendor!.uuidString)
        
        // Provided by JellyfinClient
        do{
            
            let response = try await jellyfinClient.send(Paths.getCurrentUser)
            
            if response.value.id != nil {
                print("Stored auth token is valid")
                sessionData.currentUserObject = response.value
                sessionData.jellyfinClient = jellyfinClient
                sessionData.authHeader = response.originalRequest?.allHTTPHeaderFields?["Authorization"] ?? "failed to get auth headers on token check"
                //print(response.originalRequest?.allHTTPHeaderFields)
                
                
                
                
                if !sessionData.cacheRefreshing{
                    sessionData.cacheRefreshing = true
                }
                
            }else{
                tokenCheckFailed()
            }
            
        }catch{
            print(error)
            tokenCheckFailed()
        }
            
    }
    
    func tokenCheckFailed(){
        print("No auth token, opening login")
        sessionData.isLoggedIn = false
        self.selectedTab = "Settings"
    }
    
    public func refreshCache() async {
         
        do{
            let responseAlbum = try await sessionData.jellyfinClient?.send(Paths.getItems(parameters: Paths.GetItemsParameters(userID: sessionData.currentUserObject?.id, isRecursive: true, parentID: sessionData.currentLibrary, includeItemTypes: [BaseItemKind.musicAlbum],sortBy: ["DateCreated"])))
            
            let responseSong = try await sessionData.jellyfinClient?.send(Paths.getItems(parameters: Paths.GetItemsParameters(userID: sessionData.currentUserObject?.id, isRecursive: true, parentID: sessionData.currentLibrary, includeItemTypes: [BaseItemKind.audio] )))
            
            let responseArtist = try await sessionData.jellyfinClient?.send(Paths.getArtists(parameters: Paths.GetArtistsParameters(parentID: sessionData.currentLibrary, userID: sessionData.currentUserObject?.id)))
            
            
            sessionData.cacheTotalItems = Int((responseAlbum?.value.totalRecordCount ?? 0) + 1)
            print(sessionData.cacheTotalItems)
        
            
            
            //Refresh artists
            if responseArtist?.value.items == nil {
                print("Artist fetch failed")
            } else {
                for item in responseArtist!.value.items! {
                    
                    // Check if this item is already in the cache
                    
                    fetchedResults4.nsPredicate = NSPredicate(format: "id == %@", item.id!)
                    
                    // If it is, refresh data and check image
                    if fetchedResults4.count != 0 {
                        
                        let oldItem = fetchedResults4.first
                        
                        // ALBUM
                        
                        oldItem!.name = item.name!
                        if item.userData?.isFavorite != nil{
                            oldItem!.isFavourite = item.userData!.isFavorite!
                        }
                        
                    }else{//If not, cache it
                        
                        
                        
                        var newItem = DBArtist(context: managedObjectContext)
                        
                        // ALBUM
                        newItem.id = item.id!
                        newItem.name = item.name
                        newItem.isFavourite = item.userData!.isFavorite ?? false
                         
                        
                        //print("Fetched new meta for", item.name!)
                        
                        
                    }
                    
                }
            }
            
        
        // get albums in a given library from jellyfin
        do{
            if responseAlbum?.value.items == nil {
                print("Fetch failed")
            } else {
                for item in responseAlbum!.value.items! {
                    
                    // Check if this item is already in the cache
                    
                    fetchedResults1.nsPredicate = NSPredicate(format: "id == %@", item.id!)
                    
                    if item.artistItems?.first?.id != nil{
                        fetchedResults4.nsPredicate = NSPredicate(format: "id == %@", item.artistItems!.first!.id!)
                    }else{
                        fetchedResults4.nsPredicate = NSPredicate(format: "id == %@", "")
                    }
                    
                    
                    
                    // If it is, refresh data and check image
                    if fetchedResults1.count != 0 {
                        
                        let oldItem = fetchedResults1.first
                        
                        // ALBUM
                        
                        oldItem!.name = item.name!
                        oldItem!.genres = item.genres
                        oldItem!.year = item.productionYear ?? -1
                        if fetchedResults4.count != 0 {
                            oldItem!.artist = fetchedResults4.first
                        }
                        
                        // IMAGE
                        
                        if oldItem!.image == nil && item.imageTags?["Primary"] != nil {
                            //If no image in cache and is image on server, download image
                            do{
                                
                                let response = try await sessionData.jellyfinClient?.send(Paths.getItemImage(itemID: item.id!, imageType: "Primary"))
                                
                                let newImage = DBImage(context: managedObjectContext)
                                newImage.id = item.imageTags!["Primary"]
                                
                                //Compressing and resizing recieved image, if compressed image is smaller in size than orginal, saving it
                                var compressedData = resizeImage(image: UIImage(data: response!.value)!, targetSize: CGSize(width: 512, height: 512)).jpegData(compressionQuality: 0.5)
                                if compressedData != nil{
                                    if compressedData!.count < response!.value.count {
                                        newImage.data = compressedData
                                    } else {
                                        newImage.data = response!.value
                                    }
                                }else{
                                    newImage.data = response!.value
                                    print("compression failed, saved source")
                                }
                                
                                oldItem!.image = newImage
                                
                                //print("Fetched image for", item.name!)
                                
                            }catch{
                                print("here1", error.localizedDescription)
                            }
                        } else if oldItem!.image?.id != nil {
                            if oldItem!.image!.id != item.imageTags!["Primary"] && item.imageTags?["Primary"] != nil {
                                // if is image in cahce but different image on server, download new image
                                do{
                                    
                                    let response = try await sessionData.jellyfinClient?.send(Paths.getItemImage(itemID: item.id!, imageType: "Primary"))
                                    
                                    oldItem!.image!.id = item.imageTags!["Primary"]
                                    
                                    var compressedData = resizeImage(image: UIImage(data: response!.value)!, targetSize: CGSize(width: 512, height: 512)).jpegData(compressionQuality: 0.5)
                                    if compressedData != nil{
                                        if compressedData!.count < response!.value.count {
                                            oldItem!.image!.data = compressedData
                                        } else {
                                            oldItem!.image!.data = response!.value
                                        }
                                    }else{
                                        oldItem!.image!.data = response!.value
                                        print("compression failed, saved source")
                                    }
                                    
                                    //print("Refreshed image for", item.name!)
                                    
                                }catch{
                                    print("here2", error.localizedDescription)
                                }
                            } else if oldItem!.image!.id == item.imageTags!["Primary"]{
                                //print("Image is up to date for", item.name!)
                            } else {
                                print("1 No image on server for", item.name!)
                                //print(oldItem!.image!.id, item.imageTags!["Primary"])
                            }
                        } else {
                            print("2 No image on server for", item.name!)
                        }
                        
                        //PersistenceController.shared.save()
                        
                    }else{//If not, cache it
                        
                        //print(fetchedResults1.isEmpty, item.id)
                        
                        var newItem = DBAlbum(context: managedObjectContext)
                        
                        // ALBUM
                        newItem.id = item.id
                        newItem.name = item.name!
                        newItem.genres = item.genres
                        newItem.year = item.productionYear ?? -1
                        if fetchedResults4.count != 0 {
                            newItem.artist = fetchedResults4.first
                        }
                        
                        
                        //print("Fetched new meta for", item.name!)
                        
                        // IMAGE
                        
                        if item.imageTags?["Primary"] != nil{
                            do{
                                let response = try await sessionData.jellyfinClient?.send(Paths.getItemImage(itemID: item.id!, imageType: "Primary"))
                                let newImage = DBImage(context: managedObjectContext)
                                newImage.id = item.imageTags!["Primary"]
                                newImage.data = response!.value
                                
                                var compressedData = resizeImage(image: UIImage(data: response!.value)!, targetSize: CGSize(width: 512, height: 512)).jpegData(compressionQuality: 0.5)
                                if compressedData != nil{
                                    if compressedData!.count < response!.value.count {
                                        newImage.data = compressedData
                                    } else {
                                        newImage.data = response!.value
                                    }
                                }else{
                                    newImage.data = response!.value
                                    print("compression failed, saved source")
                                }
                                                                
                                newItem.image = newImage
                                
                                //print("Fetched image for", item.name!)
                            }catch{
                                print(error)
                            }
                        }
                        
                        //PersistenceController.shared.save()
                        
                    }
                    
                    sessionData.cacheCurrentItems += 1
                    
                }
            }
        }catch{
            print(error)
        }
        
        // get songs in a given library from jellyfin
            
            if responseSong?.value.items == nil {
                print("Fetch failed")
            } else {
                
                for item in responseSong!.value.items! {
                    
                    // Check if this item is already in the cache
                    
                    fetchedResults2.nsPredicate = NSPredicate(format: "id == %@", item.id!)
                    
                    if item.artistItems?.first?.id != nil{
                        fetchedResults4.nsPredicate = NSPredicate(format: "id == %@", item.artistItems!.first!.id!)
                    }else{
                        fetchedResults4.nsPredicate = NSPredicate(format: "id == %@", "")
                    }
                    
                    //print(fetchedResults2.count)
                    
                    // If it is, refresh data and check image
                    if fetchedResults2.count != 0 {
                        
                        var oldItem = fetchedResults2.first
                        
                        oldItem!.name = item.name!
                        oldItem!.genres = item.genres
                        oldItem!.indexNumber = item.indexNumber ?? -1
                        oldItem!.lastPlayedDate = item.userData?.lastPlayedDate
                        oldItem!.isFavourite = item.userData?.isFavorite ?? false
                        oldItem!.playCount = item.userData?.playCount ?? -1
                        if fetchedResults4.count != 0 {
                            oldItem!.artist = fetchedResults4.first
                        }
                        
                    }else{//If not, cache it
                        
                        
                        fetchedResults1.nsPredicate = NSPredicate(format: "id == %@", item.albumID ?? "no id")
                        
                        if fetchedResults1.isEmpty {
                            print("Failed to find parent for song", item.id!)
                        }else{
                            
                            var newSong = DBSong(context: managedObjectContext)
                            
                            newSong.id = item.id!
                            newSong.name = item.name!
                            newSong.genres = item.genres
                            newSong.indexNumber = item.indexNumber ?? -1
                            newSong.isFavourite = item.userData?.isFavorite ?? false
                            newSong.lastPlayedDate = item.userData?.lastPlayedDate
                            if fetchedResults4.count != 0 {
                                newSong.artist = fetchedResults4.first
                            }
                            
                            fetchedResults1.first!.addToSongs(newSong)
                            
                        }
                    }
                    
                }
                
                sessionData.cacheCurrentItems += 1
                
            }
            
        
            
        } catch {
            print(error)
        }
        
        PersistenceController.shared.save()
        sessionData.cacheRefreshing = false
        sessionData.cacheTotalItems = 0
        sessionData.cacheCurrentItems = 0
        print("finised caching")
            
            
        
    }
    
    func getSongsInAlbum(parentId:String) async -> [BaseItemDto]? {
        
        var result:[BaseItemDto]?
        
        do{
            let response = try await sessionData.jellyfinClient?.send(Paths.getItems(parameters: Paths.GetItemsParameters(userID: sessionData.currentUserObject?.id, isRecursive: true, parentID: parentId, includeItemTypes: [BaseItemKind.audio] )))
            result = response?.value.items
        }catch{
            print(error)
        }
        return result
        
        /*
         var songs = await getSongsInAlbum(parentId: item.id!)
         
         if songs != nil {
             
             for song in songs! {
                 
                 //If this album does not have this song
                 
                 if !oldItem!.songs!.contains(where: {($0 as AnyObject).id == song.id}){
                     
                     var newSong = DBSong()
                     
                     newSong.id = song.id!
                     newSong.name = song.name ?? "Unknown song"
                     newSong.genres = song.genres
                     newSong.indexNumber = song.indexNumber ?? -1
                     newSong.isFavourite = song.userData?.isFavorite ?? false
                     newSong.lastPlayedDate = song.userData?.lastPlayedDate
                     oldItem?.addToSongs(newSong)
                 }
             }
             
         } else {
             print("No songs in the album of failed to fetch")
         }
         */
        
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
       let size = image.size
       
       let widthRatio  = targetSize.width  / size.width
       let heightRatio = targetSize.height / size.height
       
       // Figure out what our orientation is, and use that to form the rectangle
       var newSize: CGSize
       if(widthRatio > heightRatio) {
           newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
       } else {
           newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
       }
       
       // This is the rect that we've calculated out and this is what is actually used below
       let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
       
       // Actually do the resizing to the rect using the ImageContext stuff
       UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
       image.draw(in: rect)
       let newImage = UIGraphicsGetImageFromCurrentImageContext()
       UIGraphicsEndImageContext()
       
       return newImage!
   }
    
    func setupRemoteTransportControls() {
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()

        // Add handler for Play Command
        commandCenter.playCommand.addTarget { [] event in
            if sessionData.audioPlayer.rate == 0.0 {
                sessionData.audioPlayer.play()
                return .success
            }
            return .commandFailed
        }

        // Add handler for Pause Command
        commandCenter.pauseCommand.addTarget { [] event in
            if sessionData.audioPlayer.rate == 1.0 {
                sessionData.audioPlayer.pause()
                return .success
            }
            return .commandFailed
        }
        
        commandCenter.nextTrackCommand.addTarget { [] event in
            do{
                try sessionData.audioPlayer.next()
                return .success
            } catch {
                print(error)
                return .commandFailed
            }
            
        }
        
        commandCenter.previousTrackCommand.addTarget { [] event in
            do{
                try sessionData.audioPlayer.previous()
                return .success
            } catch {
                print(error)
                return .commandFailed
            }
        }
        
        commandCenter.changePlaybackPositionCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in

            if let changePlaybackPositionCommandEvent = event as? MPChangePlaybackPositionCommandEvent
            {
                let positionTime = changePlaybackPositionCommandEvent.positionTime
                sessionData.audioPlayer.seek(to: positionTime)
                return .success
            } else {
                return .commandFailed
            }
        }
        
        
    }
    
    
    // MARK: Monitoring playback
    
    
    func addPeriodicTimeObserver() {
        /*
        // Notify every half second
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.5, preferredTimescale: timeScale)

        timeObserverToken1 = sessionData.audioPlayer.addPeriodicTimeObserver(forInterval: time, queue: .main) { time in
            // update player transport UI
            //print(time)
            sessionData.elapsedTime = time
            
            if sessionData.audioPlayer.rate == 1.0 {
                if !self.isPlaying {
                    print("playing")
                    self.isPlaying = true
                }
            } else {
                if self.isPlaying {
                    print("paused")
                    self.isPlaying = false
                }
            }
        }*/
    }
    
    func addPlaybackBegunBoundaryTimeObserver() {
        /*
        timeObserverToken2 = sessionData.audioPlayer.addBoundaryTimeObserver(
            forTimes: [0.1 as NSValue],
            queue: DispatchQueue.main) {
                
                if self.duration == -1 {
                    
                    if sessionData.audioPlayer.currentItem?.duration != nil {
                        if sessionData.audioPlayer.currentItem!.duration.value != 0 {
                            print("valid")
                            self.duration = sessionData.audioPlayer.currentItem!.duration.seconds
                        } else {
                            print("INvalid")
                            self.duration = -1
                        }
                    }else{
                        print("no time")
                        self.duration = -1
                    }
                    
                    print(self.duration)
                    
                }
                
                
            }*/
    }
    
    func nullifyNowPlayingData(){
        
        print(sessionData.getQ())
        
        if sessionData.getQ().count <= 1{
            print("nullifyNowPlayingData()")
            sessionData.nowPlayingItem = nil
            sessionData.nowPlayingImage = nil
            
        } else {
            print("q not empty")
        }
        
        setupNowPlaying()
        
    }
    
    public func getNowPlayingData() async {
        print("getting new data")
        
        
        let id = sessionData.audioPlayer.currentItem?.getSourceUrl().split(separator: "/").suffix(2).first?.description ?? "error: failed to get now playing id"
        fetchedResults3.nsPredicate = NSPredicate(format: "id == %@", id)
        
        if !fetchedResults3.isEmpty {
            sessionData.nowPlayingItem = fetchedResults3.first
            
            
            if fetchedResults3.first?.parent?.image?.data != nil {
                sessionData.nowPlayingImage = fetchedResults3.first?.parent?.image?.data
            } else {
                print("no image found for", id)
            }
            
        } else {
            print("no song found for", id)
            
            nullifyNowPlayingData()
        }
        
        setupNowPlaying()
    }
    

    
    //----------------------------
    
    
    
    func setupNowPlaying() {
        
        print("SETTING NOW PLAYING")
        
        // Define Now Playing Info
        var nowPlayingInfo = [String : Any]()
        
        nowPlayingInfo[MPMediaItemPropertyTitle] = sessionData.nowPlayingItem?.name
        
        nowPlayingInfo[MPMediaItemPropertyArtist] = sessionData.nowPlayingItem?.artist?.name ?? sessionData.nowPlayingItem?.parent?.artist?.name
        
        if self.nowPlayingImage != nil{
            if let image = UIImage(data: sessionData.nowPlayingImage!) {
                nowPlayingInfo[MPMediaItemPropertyArtwork] =
                    MPMediaItemArtwork(boundsSize: image.size) { size in
                        // Extension used here to return newly sized image
                        return image.imageWith(newSize: size)
                }
            }
        } else {
            nowPlayingInfo[MPMediaItemPropertyArtwork] = nil
        }
        
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = sessionData.audioPlayer.currentTime
        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = sessionData.audioPlayer.duration
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = sessionData.audioPlayer.rate
        
        // Set the metadata
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        
        
        
    }
    
    
    
}



extension UIImage {
    func imageWith(newSize: CGSize) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let image = renderer.image { _ in
            self.draw(in: CGRect.init(origin: CGPoint.zero, size: newSize))
        }
        return image.withRenderingMode(self.renderingMode)
    }
}




/*
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
*/
