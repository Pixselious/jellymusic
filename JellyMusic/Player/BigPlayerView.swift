//
//  BigPlayerView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 24.07.2022.
//

import SwiftUI
import UIKit
import JellyfinAPI
import Combine
import SwiftAudioPlayer
import MediaPlayer
import AVFAudio


struct BigPlayerView: View {
    
    @EnvironmentObject var sessionData: SessionData
    
    @State var imageURL:String = ""
    @State var song:BaseItemDto?
    @State var playStatus = sessionData.audioPlayer.p
    
    @State var isFavourite = false
    
    @State var timeSlider = 0.0
    @State private var isEditing = false
    
    @State private var soundLevel: Float = AVAudioSession.sharedInstance().outputVolume
    
    @Binding var imageData: Data?
    
    @State private var disposables = Set<AnyCancellable>()
    
    var body: some View {
            //MARK: Image
            VStack{
                
                if imageURL != "" && imageData != nil && sessionData.nowPlaying != "---##$$CLEAR$$##---"{
                    Image(uiImage: UIImage(data: imageData!)!)
                        .resizable()
                        .scaledToFit()
                        .cornerRadius(10)
                        .frame(maxWidth: 1280)
                        .padding(.all, playStatus == .playing ? 30 : 70)
                        .shadow(color: .gray, radius: 1)
                        .animation(.easeInOut)
                } else {
                    ZStack {
                        
                        Image(systemName: "square.fill")
                            .resizable()
                            .scaledToFit()
                            .foregroundColor(.gray)
                        
                        Image(systemName: "record.circle.fill")
                            .resizable()
                            .padding(.all)
                            .scaledToFit()
                    }
                    .padding(.all, playStatus == .playing ? 30 : 70)
                }
                
                //MARK: Title, artist and menu button
                HStack{
                    
                    VStack(alignment: .leading){
                        //Song title
                        if song?.name != nil && sessionData.nowPlaying != "---##$$CLEAR$$##---"{
                            Text(song!.name!)
                                .font(.title3)
                                .fontWeight(.semibold)
                                .foregroundColor(.white)
                                .lineLimit(1)
                            
                        } else {
                            RoundedRectangle(cornerRadius: 7)
                                .foregroundColor(.white)
                                .frame(width: 120 , height: 20)
                                .opacity(0.9)
                        }
                        
                        //Artist
                        if song?.albumArtist != nil && sessionData.nowPlaying != "---##$$CLEAR$$##---"{
                            Text(song!.albumArtist!)
                                .font(.title3)
                                .fontWeight(.regular)
                                .foregroundColor(.white)
                                .opacity(0.5)
                                .lineLimit(1)
                            
                        } else {
                            RoundedRectangle(cornerRadius: 7)
                                .foregroundColor(.white)
                                .frame(width: 120 , height: 20)
                                .opacity(0.5)
                        }
                    }
                    
                    Spacer()
                    
                    Menu{
                        if isFavourite ?? false{
                            Button{
                                unMarkFavourite()
                                
                            } label: {
                                Label("Remove from favourites", systemImage: "heart.slash")
                            }
                        } else {
                            Button{
                                markFavourite()
                                
                            } label: {
                                Label("Add to favourites", systemImage: "heart")
                            }
                        }
                    } label: {
                        ZStack{
                            Circle()
                                .frame(width: 30, height: 30)
                                .foregroundColor(.white)
                                .opacity(0.3)
                            
                            
                            Image(systemName: "ellipsis")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 17)
                                .foregroundColor(.white)
                        }.buttonStyle(.plain)
                    }
                
                }.padding(.horizontal, 30)
                
                //MARK: Slider and time
                VStack{
                    Slider(value: $timeSlider, in: 0.0...sessionData.playerDuration, onEditingChanged: {editing in isEditing = editing})
                        .opacity(0.8)
                        .tint(.init(.sRGB, white: 1, opacity: 0.5))
                    
                    
                    HStack{
                        
                        if SAPlayer.shared.prettyElapsedTime != nil && SAPlayer.shared.prettyDuration != nil{
                            if SAPlayer.shared.prettyDuration!.first == "0"{
                                Text(SAPlayer.shared.prettyElapsedTime!.dropFirst(2))
                                    .font(.footnote)
                                    .foregroundColor(.white)
                                    .opacity(0.5)
                                    .multilineTextAlignment(.center)
                                
                                Spacer()
                                
                                Text(SAPlayer.shared.prettyDuration?.dropFirst(2) ?? "00:00")
                                    .font(.footnote)
                                    .foregroundColor(.white)
                                    .opacity(0.5)
                                    .multilineTextAlignment(.center)
                                
                            } else {
                                Text(SAPlayer.shared.prettyElapsedTime!)
                                    .font(.footnote)
                                    .foregroundColor(.white)
                                    .opacity(0.5)
                                    .multilineTextAlignment(.center)
                                
                                Spacer()
                                
                                Text(SAPlayer.shared.prettyDuration ?? "0:00:00")
                                    .font(.footnote)
                                    .foregroundColor(.white)
                                    .opacity(0.5)
                                    .multilineTextAlignment(.center)
                            }
                        } else {
                            Text("00:00")
                                .font(.footnote)
                                .foregroundColor(.white)
                                .opacity(0.5)
                                .multilineTextAlignment(.center)
                            
                            Spacer()
                            
                            Text("00:00")
                                .font(.footnote)
                                .foregroundColor(.white)
                                .opacity(0.5)
                                .multilineTextAlignment(.center)
                        }
                    }
                
                    
                }.padding(.horizontal, 30)
                
                Spacer() 
                
                //MARK: Media controls
                
                HStack{
                    Spacer()
                    Button{
                        SAPlayer.shared.skipBackwards()
                    } label: {
                        ZStack {
                            
                            Circle()
                                .frame(width: 60, height: 60)
                                .opacity(0)
                                .foregroundColor(.gray)
                            
                            Image(systemName: "backward.fill")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 40)
                                .foregroundColor(.white)
                        }
                    }
                    Spacer()
                    Button{
                        SAPlayer.shared.togglePlayAndPause()
                    } label: {
                        switch playStatus{
                        case .playing:
                            ZStack {
                                Circle()
                                    .frame(width: 60, height: 60)
                                    .opacity(0)
                                
                                Image(systemName: "pause.fill")
                                    .resizable()
                                    .scaledToFit()
                                .frame(height: 40)
                                .foregroundColor(.white)
                            }
                            
                        default:
                            ZStack {
                                
                                Circle()
                                    .frame(width: 60, height: 60)
                                    .opacity(0)
                                
                                Image(systemName: "play.fill")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(height: 40)
                                    .foregroundColor(.white)
                            }
                        }
                    }
                    Spacer()
                    Button{
                        SAPlayer.shared.skipForward()
                    } label: {
                        ZStack {
                            
                            Circle()
                                .frame(width: 60, height: 60)
                                .opacity(0)
                                .foregroundColor(.gray)
                            
                            Image(systemName: "forward.fill")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 40)
                                .foregroundColor(.white)
                        }
                    }
                    Spacer()
                }
                
                Spacer()
                
                //MARK: Volume slider
                HStack {
                    
                    Image(systemName: "speaker.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(height: 10)
                        .foregroundColor(.white)
                        .opacity(0.4)
                        .padding(.trailing)
                    
                    Slider(value: $soundLevel, in: 0...1)
                        .opacity(0.4)
                        .tint(.init(.sRGB, white: 1, opacity: 0.3))
                    
                    Image(systemName: "speaker.wave.3.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(height: 10)
                        .foregroundColor(.white)
                        .opacity(0.4)
                        .padding(.leading)
                    
                }
                .padding(.horizontal, 30)
                
                //MARK: Bottom row
                HStack {
                    AirPlayButton()
                        .frame(width: 40, height: 40)
                        .opacity(0.5)
                }
                    .padding(.horizontal, 30)
                
                
            }
            .onAppear(){
                
                soundLevel = AVAudioSession.sharedInstance().outputVolume
                
                SAPlayer.Updates.PlayingStatus.subscribe({status in
                    playStatus = status
                    print(status)
                })
                
                let item = sessionData.nowPlaying
                //get album art
                
                /*
                LibraryAPI.getAncestors(itemId: item ?? "", userId: sessionData.authResult?.user?.id)
                    .subscribe(on: DispatchQueue.global())
                    .receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { value in
                        switch value {
                        case .failure (let error):
                            print("Couldn't fetch now playing parent")
                            print(error)
                        case .finished:
                            break
                        }
                    }, receiveValue: { result in
                        
                            ImageAPI.getItemImage(itemId: result[0].id!, imageType: .primary)
                                .subscribe(on: DispatchQueue.global())
                                .receive(on: DispatchQueue.main)
                                .sink(receiveCompletion: { value in
                                    switch value {
                                    case .failure (let error):
                                        print(error)
                                        print("Couldn't fetch now playing image")
                                        imageURL = ""
                                    case .finished:
                                        break
                                    }
                                }, receiveValue: { image in
                                    imageURL = "\(image)"
                                })
                                .store(in: &disposables)
                        
                    })
                    .store(in: &disposables)
                
                //get data about item
                ItemsAPI.getItemsByUserId(userId: sessionData.authResult?.user?.id ?? "", ids: [item ?? ""])
                    .subscribe(on: DispatchQueue.global())
                    .receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { value in
                        switch value {
                        case .failure (let error):
                            print("Couldn't fetch now playing item")
                            print(error)
                        case .finished:
                            break
                        }
                    }, receiveValue: { result in
                        song = result.items?[0]
                        isFavourite = result.items?[0].userData?.isFavorite ?? false
                    })
                    .store(in: &disposables)*/
                
            }
            .onChange(of: soundLevel, perform: {level in
                MPVolumeView.setVolume(level)
            })
            // The one below is the important one
            .onChange(of: sessionData.nowPlaying, perform: { item in
                
                /*
                
                //get album art
                LibraryAPI.getAncestors(itemId: item ?? "", userId: sessionData.authResult?.user?.id)
                    .subscribe(on: DispatchQueue.global())
                    .receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { value in
                        switch value {
                        case .failure (let error):
                            print("Couldn't fetch now playing parent")
                            print(error)
                        case .finished:
                            break
                        }
                    }, receiveValue: { result in
                        
                            ImageAPI.getItemImage(itemId: result[0].id!, imageType: .primary)
                                .subscribe(on: DispatchQueue.global())
                                .receive(on: DispatchQueue.main)
                                .sink(receiveCompletion: { value in
                                    switch value {
                                    case .failure (let error):
                                        print(error)
                                        print("Couldn't fetch now playing image")
                                        imageURL = ""
                                    case .finished:
                                        break
                                    }
                                }, receiveValue: { image in
                                    imageURL = "\(image)"
                                    
                                    //here
                                    
                                })
                                .store(in: &disposables)
                        
                    })
                    .store(in: &disposables)
                
                //get data about item
                ItemsAPI.getItemsByUserId(userId: sessionData.authResult?.user?.id ?? "", ids: [item ?? ""])
                    .subscribe(on: DispatchQueue.global())
                    .receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { value in
                        switch value {
                        case .failure (let error):
                            print("Couldn't fetch now playing item")
                            print(error)
                        case .finished:
                            break
                        }
                    }, receiveValue: { result in
                        song = result.items?[0]
                        isFavourite = result.items?[0].userData?.isFavorite ?? false
                    })
                    .store(in: &disposables)*/
                
            })
            .background(){
                BubbleView(imageData: $imageData)
                    .ignoresSafeArea()
            }
    }
    
    func markFavourite(){
        /*UserLibraryAPI.markFavoriteItem(userId: (sessionData.authResult?.user?.id)!, itemId: sessionData.nowPlaying ?? "")
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { value in
                switch value {
                case .failure (let error):
                    print(error)
                case .finished:
                    break
                }
            }, receiveValue: { result in
                isFavourite = true
            })
            .store(in: &disposables)*/
    }
    
    func unMarkFavourite(){/*
        UserLibraryAPI.unmarkFavoriteItem(userId: (sessionData.authResult?.user?.id)!, itemId: sessionData.nowPlaying ?? "")
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { value in
                switch value {
                case .failure (let error):
                    print(error)
                case .finished:
                    break
                }
            }, receiveValue: { result in
                isFavourite = false
            })
            .store(in: &disposables)*/
    }
    
    //MARK: AirPlay views
    struct AirPlayButton: UIViewControllerRepresentable {
        func makeUIViewController(context: UIViewControllerRepresentableContext<AirPlayButton>) -> UIViewController {
            return AirPLayViewController()
        }

        func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<AirPlayButton>) {

        }
    }

    class AirPLayViewController: UIViewController {

        override func viewDidLoad() {
            super.viewDidLoad()

            let button = UIButton()
            let boldConfig = UIImage.SymbolConfiguration(scale: .large)
            let boldSearch = UIImage(systemName: "airplayaudio", withConfiguration: boldConfig)

            button.setImage(boldSearch, for: .normal)
            button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            //button.backgroundColor = .red
            button.tintColor = .white

            button.addTarget(self, action: #selector(self.showAirPlayMenu(_:)), for: .touchUpInside)
            self.view.addSubview(button)
        }

        @objc func showAirPlayMenu(_ sender: UIButton){ // copied from https://stackoverflow.com/a/44909445/7974174
            let rect = CGRect(x: 0, y: 0, width: 0, height: 0)
            let airplayVolume = MPVolumeView(frame: rect)
            airplayVolume.showsVolumeSlider = false
            self.view.addSubview(airplayVolume)
            for view: UIView in airplayVolume.subviews {
                if let button = view as? UIButton {
                    button.sendActions(for: .touchUpInside)
                    break
                }
            }
            airplayVolume.removeFromSuperview()
        }
    }
    
    
}

extension View {
// This function changes our View to UIView, then calls another function
// to convert the newly-made UIView to a UIImage.
    public func asUIImage() -> UIImage {
        let controller = UIHostingController(rootView: self)
        
        controller.view.frame = CGRect(x: 0, y: CGFloat(Int.max), width: 1, height: 1)
        UIApplication.shared.windows.first!.rootViewController?.view.addSubview(controller.view)
        
        let size = controller.sizeThatFits(in: UIScreen.main.bounds.size)
        controller.view.bounds = CGRect(origin: .zero, size: size)
        controller.view.sizeToFit()
        
// here is the call to the function that converts UIView to UIImage: `.asUIImage()`
        let image = controller.view.asUIImage()
        controller.view.removeFromSuperview()
        return image
    }
}

extension UIView {
// This is the function to convert UIView to UIImage
    public func asUIImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

extension MPVolumeView {
    static func setVolume(_ volume: Float) -> Void {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
}

/*
 {
     AsyncImage(url: URL(string: imageURL)) { image in
                 image
                   .resizable()
                   .scaledToFit()
                   .cornerRadius(10)
                   .frame(maxWidth: 1280)
                   .padding(.all, playStatus == .playing ? 30 : 70)
                   .shadow(color: .gray, radius: 1)
                   .animation(.easeInOut)
         
             
         

                 
             } placeholder: {
                     
                     ZStack {
                         
                         RoundedRectangle(cornerRadius: 10)
                             .foregroundColor(.gray)
                             .opacity(0.7)
                         
                         Image(systemName: "record.circle.fill")
                             .resizable()
                             .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                             .scaledToFit()
                         
                         .opacity(0.7)
                     }.frame(width: 200, height: 200)
                     .padding(.all, playStatus == .playing ? 30 : 70)
                 
         }
 }
 */
