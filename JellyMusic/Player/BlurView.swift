//
//  BlurView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 22.07.2022.
//

import SwiftUI

struct BlurView: UIViewRepresentable {
    func makeUIView (context: Context) -> UIVisualEffectView{
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .systemChromeMaterial))
        
        return view
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {}
    }

