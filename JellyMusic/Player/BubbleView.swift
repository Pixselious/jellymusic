//
//  BubbleView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 24.07.2022.
//

import SwiftUI
import UIKit
import ColorKit

struct BubbleView: View {
    //@EnvironmentObject var sessionData: SessionData
    @State var scale : CGFloat = 1
    @Binding var imageData: Data?
    @State var colors:[UIColor] = []
    @State var average:UIColor?
    @State var entitys: Int = 3
    
    var body: some View {
        
        
        ZStack {
            
            Rectangle()
                .foregroundColor(Color(uiColor: average ?? UIColor(red: 0, green: 0, blue: 0, alpha: 1)))
                .saturation(2.0)
            ForEach(0..<2) { i in
                ForEach ( colors.count > 0 ? 1...colors.count : 1...entitys, id:\.self) { index in
                    Ellipse()
                        .foregroundColor(Color(uiColor: colors.count > 0 ?  colors[index-1] : UIColor(red: .random(in: 0...0.3), green: 0, blue: .random(in: 0...0.3), alpha: .random(in: 0.3...0.8))))
                        .opacity(.random(in: 0.7...0.9))
                        .blendMode(.normal) //https://www.fivestars.blog/articles/swiftui-blend-modes/
                        .saturation(2.0)
                        .frame(width: .random(in: 200...250),
                               height: .random (in:200...350),
                               alignment: .center)
                        .position(CGPoint(x: .random(in: 0...UIScreen.main.bounds.width), y: .random(in:0...UIScreen.main.bounds.height)))
                    
                }
            }
            .blur(radius: 50)
            
            Rectangle()
                .foregroundColor(.black)
                .opacity(0.2)
            
        }
        
        .onChange(of: imageData, perform: { data in
            do{
                if imageData != nil{
                    let image = UIImage(data: imageData!)
                    if image != nil{
                        print(image as Any)
                        colors = try image!.alpha(1).dominantColors()
                        average = try image!.alpha(1).averageColor()
                        if colors.count > 0 {
                            entitys = colors.count
                        }
                    }
                }
                else {
                    print("Colors image conversion failed", imageData as Any)
                    colors = []
                    average = nil
                }
            } catch{
                print("Colors failed")
            }
            
        })
        .onAppear {
            self.scale = 1 //1.2 is the default circle scale
            //print("appeared")
            do{
                if imageData != nil{
                    let image = UIImage(data: imageData!)
                    if image != nil{
                        //print(image as Any)
                        var pallete = try ColorPalette(orderedColors: image!.alpha(1).dominantColors(), ignoreContrastRatio: true)
                        colors = try image!.dominantColors()
                        average = pallete!.primary
                        if colors.count > 0 {
                            entitys = colors.count
                        } 
                    }
                }
                else {
                    print("Colors image conversion failed", imageData as Any)
                    colors = []
                    average = nil
                }
            } catch{
                print("Colors failed")
            }
            
        }
        
        .drawingGroup(opaque: false, colorMode: .linear) // makes the app faster, rendering the contents of the view into an off-screen image before putting it back onto the screen as a single rendered output
        .ignoresSafeArea()
    }
}

extension UIImage {

    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
