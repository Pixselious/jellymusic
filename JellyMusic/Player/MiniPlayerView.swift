//
//  MiniPlayerView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 23.07.2022.
//

import SwiftUI
import JellyfinAPI
import Combine
import AVFoundation


struct MiniPlayerView: View {
    @EnvironmentObject var sessionData: SessionData
    
    //Now playing
    @State var imageData:Data?
    @State var song:DBSong?
    @State var isExpanded = false
    
    @FetchRequest(sortDescriptors: [SortDescriptor(\.id)])
    private var fetchedResults: FetchedResults<DBSong>
    
    var body: some View {
        VStack{
            HStack{
                
                
                //Album art
                if imageData != nil {
                    Image(uiImage: UIImage(data: imageData!)!)
                      .resizable()
                      .scaledToFit()
                      .cornerRadius(5)
                      .frame(width: 50, height: 50)
                    
                } else {
                    ZStack {
                        RoundedRectangle(cornerRadius: 5)
                            .foregroundColor(.gray)
                            .opacity(0.7)
                            .frame(width: 50, height: 50)
                    }
                }
                

                    Spacer()
                        .frame(width: 10)
                    
                    
                    //Song title
                    if song?.name != nil && sessionData.audioPlayer.currentItem != nil{
                        Text(song!.name!)
                            .lineLimit(1)
                        
                    } else {
                        RoundedRectangle(cornerRadius: 7)
                            .foregroundColor(.gray)
                            .frame(width: 120 , height: 20)
                            .opacity(0.7)
                    }

                
                Spacer()
                

                    
                    /*
                    //Skip back
                    Button{
                        SAPlayer.shared.skipBackwards()
                    } label: {
                        Image(systemName: "backward.fill")
                            .resizable()
                            .scaledToFit()
                    }
                    */
                
                
                    //Play Pause controls
                    Button{                        
                        sessionData.audioPlayer.togglePlaying()
                        
                    } label: {
                            ZStack {
                                Circle()
                                    .frame(width: 40, height: 40)
                                    .opacity(0)
                                
                                Image(systemName: sessionData.isPlaying ? "pause.fill" : "play.fill")
                                    .resizable()
                                    .scaledToFit()
                                .frame(height: 20)
                                .foregroundColor(Color("AdaptiveBlack"))
                            }
                    }
                    
                    //Skip fwd
                    Button{
                        do{
                            try sessionData.audioPlayer.next()
                        } catch {
                            print(error)
                        }
                    } label: {
                        ZStack {
                            
                            Circle()
                                .frame(width: 40, height: 40)
                                .opacity(0)
                                .foregroundColor(.gray)
                            
                            Image(systemName: "forward.fill")
                                .resizable()
                                .scaledToFit()
                                .frame(height: 20)
                                .foregroundColor(Color("AdaptiveBlack"))
                        }
                    }
                

                
            }
            .padding(.horizontal)
        }
        .frame(height: 65 )
        .background(
            VStack(spacing: 0){
                BlurView()
                Divider()
            }
                .onTapGesture(){
                    withAnimation(.spring()){isExpanded.toggle()}
                }
        )
        .ignoresSafeArea()
        .offset(y: -48)
        .sheet(isPresented: $isExpanded, content: {
            BigPlayerView(imageData: $imageData, song: $song)
                .presentationDragIndicator(.hidden)
                //.offset(y:-20)
        })
        .onChange(of: sessionData.nowPlayingItem){ _ in
            self.song = sessionData.nowPlayingItem
            self.imageData = sessionData.nowPlayingImage
        }
        

    }
    

}
/*
struct MiniPlayerView_Previews: PreviewProvider {
    static var previews: some View {
        MiniPlayerView()
    }
}
*/
