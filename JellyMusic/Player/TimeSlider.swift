//
//  TimeSlider.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 27.01.2023.
//

import SwiftUI
import AVFoundation

struct TimeSlider: View {
    
    @EnvironmentObject var sessionData: SessionData
    
    @State var duration = 0.0
    @State var timeSlider = 0.0
    @State private var timeIsEditing = false
    
    
    var body: some View {
        
        VStack{
            
            Slider(value: $timeSlider, in: self.duration == -1 ? 0.0...1.0 : 0.0...self.duration, onEditingChanged: {editing in timeIsEditing = editing})
                .opacity(0.8)
                .tint(.init(.sRGB, white: 1, opacity: 0.5))
            

            
            HStack{
                
                if sessionData.audioPlayer.currentItem != nil {
                    Text(prettyfy(seconds: self.timeSlider))
                        .font(.footnote)
                        .foregroundColor(.white)
                        .opacity(0.5)
                        .multilineTextAlignment(.center)
                    
                    Spacer()
                    
                    Text(prettyfy(seconds: self.duration == -1 ? 0 : self.timeSlider - self.duration))
                        .font(.footnote)
                        .foregroundColor(.white)
                        .opacity(0.5)
                        .multilineTextAlignment(.center)
                    
                } else {
                    Text("00:00")
                        .font(.footnote)
                        .foregroundColor(.white)
                        .opacity(0.5)
                        .multilineTextAlignment(.center)
                    
                    Spacer()
                    
                    Text("00:00")
                        .font(.footnote)
                        .foregroundColor(.white)
                        .opacity(0.5)
                        .multilineTextAlignment(.center)
                }
            }
            
        }
        .padding(.horizontal, 30)
        .onChange(of: timeIsEditing){ isEditing in
            if isEditing == false {
                //print(CMTime(seconds: timeSlider, preferredTimescale: 1))
                sessionData.audioPlayer.seek(to: timeSlider)
            }
        }
        .onChange(of: sessionData.elapsedTime){ time in
            if timeIsEditing == false {
                timeSlider = time
            }
        }
        .onChange(of: sessionData.audioPlayer.duration){ time in
            self.duration = time
        }
        .onAppear(){
            self.duration = sessionData.audioPlayer.duration
        }
        
    }
    
    func prettyfy(seconds: Double) -> String {
        
        var value = seconds
        var isNegative = false
        
        if value < 0 {
            value.negate()
            isNegative = true
        }
        
        var pretty:String
        
        let sec = Int(Int(value) % 60)
        let min = Int((Int(value) / 60) % 60)
        let hr = Int(value / 3600)
        
        if hr <= 0 {
            pretty = "\(isNegative ? "-" : "")\(min <= 9 ? "0" : "" )\(min):\(sec <= 9 ? "0" : "")\(sec)"
        } else {
            pretty = "\(isNegative ? "-" : "")\(hr):\(min <= 9 ? "0" : "" )\(min):\(sec <= 9 ? "0" : "")\(sec)"
        }
        
        return pretty
    }
    
}

/*
struct TimeSlider_Previews: PreviewProvider {
    static var previews: some View {
        TimeSlider()
    }
}
*/
