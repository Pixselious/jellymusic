//
//  ArtistView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 28.01.2023.
//

import SwiftUI
import JellyfinAPI

struct ArtistView: View {
    
    @EnvironmentObject var sessionData: SessionData
    
    @State var artistId: String
    @State var artistName: String
    
    @FetchRequest(sortDescriptors: [NSSortDescriptor(key: "year", ascending: false), NSSortDescriptor(key: "name", ascending: true)])
    var fetchedResults: FetchedResults<DBAlbum>
    
    @State var frozenResults: FetchedResults<DBAlbum>?
    
    
    
    var body: some View {
        ScrollView{
            if frozenResults != nil{
                LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())]){
                    ForEach(frozenResults!, id: \.self){ album in
                        NavigationLink(destination:
                                        AlbumListView(album: album) ){
                            AlbumCardView(album: album, imageData: album.image?.data)
                                .padding(.all, 5.0)
                        }
                                        .buttonStyle(PlainButtonStyle())
                    }
                    
                }
                .padding(.bottom, 100)
                .padding(.horizontal, 12)
            }
        }
        .onAppear(){
            fetchedResults.nsPredicate = NSPredicate(format: "artist.id == %@", artistId)
            self.frozenResults = self.fetchedResults
        }
        
        .navigationTitle(artistName)
        
    }
}

struct ArtistView_Previews: PreviewProvider {
    static var previews: some View {
        ArtistView(artistId: "Test artist", artistName: "Test artist")
    }
}
