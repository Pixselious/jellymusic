//
//  BigPlayerView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 24.07.2022.
//

import SwiftUI
import UIKit
import JellyfinAPI
import MediaPlayer
import AVFoundation


struct BigPlayerView: View {
    
    @EnvironmentObject var sessionData: SessionData
    
    @Binding var imageData:Data?
    @Binding var song:DBSong?
    
    @State var imageExpanded = false
    
    
    @State private var volumeIsEditing = false
    
    @State private var soundLevel: Float = AVAudioSession.sharedInstance().outputVolume
    
    
    var body: some View {
            
            VStack{
                
                //MARK: Image
                
                ZStack(alignment: .center){
                    
                    Image(systemName: "square.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: 1280)
                        .opacity(0)
                    
                    if imageData != nil {
                        
                        Image(uiImage: UIImage(data: imageData!)!)
                            .resizable()
                            .scaledToFit()
                            .cornerRadius(10)
                            .frame(maxWidth: 1280)
                            .padding(.all, imageExpanded ? 22 : 70)
                            .shadow(radius: imageExpanded ? 10 : 5)
                            .onChange(of: sessionData.isPlaying){state in
                                withAnimation(.easeInOut(duration: 0.3)){
                                    imageExpanded = state
                                }
                            }
                        
                    } else {
                        withAnimation(.easeInOut){
                            ZStack {
                                
                                Image(systemName: "square.fill")
                                    .resizable()
                                    .scaledToFit()
                                    .foregroundColor(.gray)
                                
                                Image(systemName: "record.circle.fill")
                                    .resizable()
                                    .padding(.all)
                                    .scaledToFit()
                            }
                            .frame(maxWidth: 1280)
                            .padding(.all, imageExpanded ? 22 : 70)
                            .onChange(of: sessionData.isPlaying){state in
                                withAnimation(.easeInOut(duration: 0.3)){
                                    imageExpanded = state
                                }
                            }
                        }
                    }
                    
                }
                
                //MARK: Title, artist and menu button
                HStack{
                    
                    VStack(alignment: .leading){
                        //Song title
                        if song?.name != nil{
                            Text(song!.name!)
                                .font(.title3)
                                .fontWeight(.semibold)
                                .foregroundColor(.white)
                                .lineLimit(1)
                            
                        } else {
                            RoundedRectangle(cornerRadius: 7)
                                .foregroundColor(.white)
                                .frame(width: 170 , height: 20)
                                .opacity(0.9)
                        }
                        
                        //Artist
                        if song?.artist?.name != nil || song?.parent?.artist?.name != nil {
                            NavigationLink(destination: {
                                ArtistView(artistId: song?.artist?.id  ?? "Unknown artist", artistName: song?.artist?.name ?? "Unknown artist")
                            }){
                                Text(song?.artist?.name ?? song!.parent!.artist!.name!)
                                    .font(.title3)
                                    .fontWeight(.regular)
                                    .foregroundColor(.white)
                                    .opacity(0.5)
                                    .lineLimit(1)
                            }
                            
                        } else {
                            RoundedRectangle(cornerRadius: 7)
                                .foregroundColor(.white)
                                .frame(width: 120 , height: 20)
                                .opacity(0.5)
                        }
                    }
                    
                    Spacer()
                    
                    if song?.isFavourite != nil{
                        Button{
                            if song!.isFavourite {
                                unMarkFavourite()
                            } else {
                                markFavourite()
                            }
                            
                        } label: {
                            Image(systemName: song!.isFavourite ? "heart.fill" : "heart")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 24)
                                .foregroundColor(.white)
                        }
                        .buttonStyle(.plain)
                        
                        
                    }
                    
                    
                    /*
                    Menu{
                        if song?.isFavourite != nil{
                            if song!.isFavourite {
                                Button{
                                    unMarkFavourite()
                                    
                                } label: {
                                    Label("Unlove", systemImage: "heart.slash")
                                }
                            } else {
                                Button{
                                    markFavourite()
                                    
                                } label: {
                                    Label("Love", systemImage: "heart")
                                }
                            }
                        }
                    } label: {
                        ZStack{
                            Circle()
                                .frame(width: 30, height: 30)
                                .foregroundColor(.white)
                                .opacity(0.3)
                            
                            
                            Image(systemName: "ellipsis")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 17)
                                .foregroundColor(.white)
                        }.buttonStyle(.plain)
                    }
                     */
                }
                .padding(.horizontal, 30)
                
                //MARK: Slider and time
                
                TimeSlider()
                
                Spacer() 
                
                //MARK: Media controls
                
                HStack{
                    Spacer()
                    Button{
                        do{
                            try sessionData.audioPlayer.previous()
                        } catch {
                            print(error)
                        }
                    } label: {
                        ZStack {
                            
                            Circle()
                                .frame(width: 60, height: 60)
                                .opacity(0)
                                .foregroundColor(.gray)
                            
                            Image(systemName: "backward.fill")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 40)
                                .foregroundColor(.white)
                        }
                    }
                    Spacer()
                    Button{
                        sessionData.audioPlayer.togglePlaying()
                    } label: {
                            ZStack {
                                Circle()
                                    .frame(width: 60, height: 60)
                                    .opacity(0)
                                
                                Image(systemName: sessionData.isPlaying ? "pause.fill" : "play.fill")
                                    .resizable()
                                    .scaledToFit()
                                .frame(height: 40)
                                .foregroundColor(.white)
                            }
                    }
                    Spacer()
                    Button{
                        do{
                            try sessionData.audioPlayer.next()
                        } catch {
                            print(error) 
                        }
                    } label: {
                        ZStack {
                            
                            Circle()
                                .frame(width: 60, height: 60)
                                .opacity(0)
                                .foregroundColor(.gray)
                            
                            Image(systemName: "forward.fill")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 40)
                                .foregroundColor(.white)
                        }
                    }
                    Spacer()
                }
                
                Spacer()
                
                //MARK: Volume slider
                HStack {
                    
                    Image(systemName: "speaker.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(height: 10)
                        .foregroundColor(.white)
                        .opacity(0.4)
                        .padding(.trailing)
                    
                    Slider(value: $soundLevel, in: 0.0...1.0, onEditingChanged: {editing in volumeIsEditing = editing})
                        .opacity(0.4)
                        .tint(.init(.sRGB, white: 1, opacity: 0.3))
                    
                    Image(systemName: "speaker.wave.3.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(height: 10)
                        .foregroundColor(.white)
                        .opacity(0.4)
                        .padding(.leading)
                    
                }
                .padding(.horizontal, 30)
                .padding(.bottom, 60)
                
                //MARK: Bottom row
                HStack {
                    
                }
                    .padding(.horizontal, 30)
                
                
            }

        
            .onChange(of: self.soundLevel){ sound in
                MPVolumeView.setVolume(sound)
            }
            .onChange(of: AVAudioSession.sharedInstance().outputVolume){ sound in
                if volumeIsEditing == false {
                    self.soundLevel = sound
                }
            }
            .onAppear(){
                
                soundLevel = AVAudioSession.sharedInstance().outputVolume
                
                self.imageExpanded = sessionData.isPlaying
                
                //let item = sessionData.audioPlayer.currentItem
                
            }
            // The one below is the important one
            .background(){
                BubbleView(imageData: $imageData)
                    .ignoresSafeArea()
            }
    }
    
    func markFavourite(){
        /*UserLibraryAPI.markFavoriteItem(userId: (sessionData.authResult?.user?.id)!, itemId: sessionData.nowPlaying ?? "")
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { value in
                switch value {
                case .failure (let error):
                    print(error)
                case .finished:
                    break
                }
            }, receiveValue: { result in
                isFavourite = true
            })
            .store(in: &disposables)*/
    }
    
    func unMarkFavourite(){/*
        UserLibraryAPI.unmarkFavoriteItem(userId: (sessionData.authResult?.user?.id)!, itemId: sessionData.nowPlaying ?? "")
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { value in
                switch value {
                case .failure (let error):
                    print(error)
                case .finished:
                    break
                }
            }, receiveValue: { result in
                isFavourite = false
            })
            .store(in: &disposables)*/
    }
    

    
    
}
/*
extension View {
// This function changes our View to UIView, then calls another function
// to convert the newly-made UIView to a UIImage.
    public func asUIImage() -> UIImage {
        let controller = UIHostingController(rootView: self)
        
        controller.view.frame = CGRect(x: 0, y: CGFloat(Int.max), width: 1, height: 1)
        UIApplication.shared.windows.first!.rootViewController?.view.addSubview(controller.view)
        
        let size = controller.sizeThatFits(in: UIScreen.main.bounds.size)
        controller.view.bounds = CGRect(origin: .zero, size: size)
        controller.view.sizeToFit()
        
// here is the call to the function that converts UIView to UIImage: `.asUIImage()`
        let image = controller.view.asUIImage()
        controller.view.removeFromSuperview()
        return image
    }
}

extension UIView {
// This is the function to convert UIView to UIImage
    public func asUIImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
*/
extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
}


/*
 {
     AsyncImage(url: URL(string: imageURL)) { image in
                 image
                   .resizable()
                   .scaledToFit()
                   .cornerRadius(10)
                   .frame(maxWidth: 1280)
                   .padding(.all, playStatus == .playing ? 30 : 70)
                   .shadow(color: .gray, radius: 1)
                   .animation(.easeInOut)
         
             
         

                 
             } placeholder: {
                     
                     ZStack {
                         
                         RoundedRectangle(cornerRadius: 10)
                             .foregroundColor(.gray)
                             .opacity(0.7)
                         
                         Image(systemName: "record.circle.fill")
                             .resizable()
                             .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                             .scaledToFit()
                         
                         .opacity(0.7)
                     }.frame(width: 200, height: 200)
                     .padding(.all, playStatus == .playing ? 30 : 70)
                 
         }
 }
 */
