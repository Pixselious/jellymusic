//
//  AlbumCardView.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 21.01.2023.
//

import SwiftUI

struct AlbumCardView: View {
    
    @State var album: DBAlbum
    @State var imageData: Data?
    
    var body: some View {

        VStack {
            if imageData != nil {
                Image(uiImage: UIImage(data: imageData!)!)
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(7)
                    .shadow(color: .gray, radius: 0.5)
                
            } else {
                ZStack {
                    
                    RoundedRectangle(cornerRadius: 7)
                        .foregroundColor(.gray)
                        .opacity(0.7)
                    
                    Image(systemName: "record.circle.fill")
                        .resizable()
                        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                        .scaledToFit()
                        .foregroundColor(.black)
                }
            }
            HStack {
                Text(album.name!)
                    .font(.subheadline)
                    .foregroundColor(Color("AdaptiveBlack"))
                    .multilineTextAlignment(.leading)
                    .lineLimit(1)
                Spacer()
            }
            HStack {
                Text(album.artist?.name ?? "Unknown artist")
                    .font(.subheadline)
                    .foregroundColor(Color.gray)
                    .multilineTextAlignment(.leading)
                    .lineLimit(1)
                Spacer()
            }
        }
    }
}
/*
struct AlbumCardView_Previews: PreviewProvider {
    static var previews: some View {
        AlbumCardView(imageURL: "https://4kwallpapers.com/images/walls/thumbs_3t/1765.jpg", title: "Album", artist: "Artist")
    }
}
*/
