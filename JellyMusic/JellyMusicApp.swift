//
//  JellyMusicApp.swift
//  JellyMusic
//
//  Created by Матвей Писарев on 21.01.2023.
//

import SwiftUI
import JellyfinAPI
import AVFoundation
import SwiftAudioEx

let persistenceController = PersistenceController.shared

class SessionData: ObservableObject{
    
    @Published var jellyfinClient: JellyfinClient?
    
    @Published var isLoggedIn:Bool?
    @Published var currentUserName:String?
    @Published var currentUserObject:UserDto?
    @Published var currentToken:String?
    @Published var currentServerURL:String?
    @Published var currentServerID:String?
    @Published var currentLibrary:String = ""
    @Published var currentSessionInfo:SessionInfo?
    @Published var authResult:AuthenticationResult?
    @Published var authHeader:String = ""
    
    @Published var cacheRefreshing = false
    @Published var cacheTotalItems = 0
    @Published var cacheCurrentItems = 0
    
    
    // Audio player
    @Published var audioPlayer = QueuedAudioPlayer()
    
    @Published var elapsedTime = 0.0
    @Published var duration = 0.0
    
    @Published var nowPlayingItem:DBSong?
    @Published var nowPlayingImage:Data?
    
    @Published var isPlaying = false
    
    @Published var endPlaybackNotification = false
    
    public func appendToQ(url: String){ //, headers: [String : String]
        
        print(url)
        
        let item = DefaultAudioItem(audioUrl: url, sourceType: .stream)
        
        do{
            try self.audioPlayer.add(item: item)
        } catch {
            print("ERROR")
            print(error)
        }
        
        print(self.audioPlayer.items)
    }
    
    public func playNow(url: String){
        
        self.audioPlayer.stop()
        
        let item = DefaultAudioItem(audioUrl: url, sourceType: .stream)
        
        do{
            try self.audioPlayer.add(item: item, playWhenReady: true)
            self.audioPlayer.play()
        } catch {
            print("ERROR")
            print(error)
        }
    }
    
    public func appendAndPlayFirst(urls: [String], shuffle: Bool){
        
        var array = urls
        if shuffle {
            array.shuffle()
        }
        
        self.audioPlayer.stop()
        
        let first = DefaultAudioItem(audioUrl: array.first!, sourceType: .stream)
        
        do{
            try self.audioPlayer.add(item: first, playWhenReady: true)
            self.audioPlayer.play()
        } catch {
            print("ERROR")
            print(error)
        }
        
        array.remove(at: 0)
        
        
        
        var items: [DefaultAudioItem] = []
        
        for url in array {
            items.append(DefaultAudioItem(audioUrl: url, sourceType: .stream))
        }
        
        do{
            try self.audioPlayer.add(items: items, at: self.audioPlayer.currentIndex + 1)
            self.audioPlayer.play()
        } catch {
            print("ERROR")
            print(error)
        }
    }
    
    public func firstInQ(url: String){
        
        let item = DefaultAudioItem(audioUrl: url, sourceType: .stream)
        
        do{
            print("TRYYYYYYYINNNGG")
            try self.audioPlayer.add(items: [item], at: self.audioPlayer.currentIndex + 1)
        } catch {
            print("ERROR")
            print(error)
        }
        
        print(self.audioPlayer.nextItems.description)
        
    }
    
    public func getQ() -> [any AudioItem]{
        return(self.audioPlayer.nextItems)
    }
    
    public func initEventListeners(){
        self.audioPlayer.event.stateChange.addListener(self, handleStateChange)
        self.audioPlayer.event.secondElapse.addListener(self, handleSecondElapse)
        self.audioPlayer.event.updateDuration.addListener(self, handleUpdateDuration)
        self.audioPlayer.event.playbackEnd.addListener(self, handlePlaybackEnd)
    }
    
    func handleStateChange(state: AudioPlayerState){
        DispatchQueue.main.schedule {
            if state == .playing || state == .buffering {
                self.isPlaying = true
            } else {
                self.isPlaying = false
            }
        }
    }
    
    func handleSecondElapse(time: TimeInterval){
        DispatchQueue.main.schedule {
            self.elapsedTime = time
        }
    }
    
    func handleUpdateDuration(time: TimeInterval){
        DispatchQueue.main.schedule {
            self.duration = time
        }
    }
    
    func handlePlaybackEnd(reason: PlaybackEndedReason){
        DispatchQueue.main.async {
            self.endPlaybackNotification.toggle()
        }
    }


}






@main
struct JellyMusicApp: App {
    @StateObject var sessionData = SessionData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(sessionData)
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
