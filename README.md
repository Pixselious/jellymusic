# JellyMusic

JellyMusic is a music-oriented Jellyfin client written is SwiftUI. Arguably (will be) the best Jellyfin music experience on iOS.

## Features
- [x] Metadata caching
- [ ] Sorting
- [ ] Search
- [ ] Recommendations
- [ ] Offline playback
- [ ] Smart auto caching for offline playback
- [ ] Polished UI

![Player](images/IMG_7110.PNG){width=30%} ![Album](images/IMG_7113.PNG){width=30%} ![Library](images/IMG_7104.PNG){width=30%}
